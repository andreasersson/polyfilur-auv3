/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

struct EnvelopeView: View {
  let name: String
  let spacing: Double
  let attack: ObservableAUParameter
  let decay: ObservableAUParameter
  let sustain: ObservableAUParameter
  let release: ObservableAUParameter

  let curve: Float = 1.0/3.0

  var body: some View {
    GroupBox(label: Text(name)) {
      HStack(spacing: spacing) {
        ParameterView(parameter: attack)
          .parameterStyle(ParameterKnobStyle())
          .curve(curve)
        ParameterView(parameter: decay)
          .parameterStyle(ParameterKnobStyle())
          .curve(curve)
        ParameterView(parameter: sustain)
          .parameterStyle(ParameterKnobStyle())
          .curve(curve)
        ParameterView(parameter: release)
          .parameterStyle(ParameterKnobStyle())
          .curve(curve)
      }
      .padding(0)
    }
  }
}
