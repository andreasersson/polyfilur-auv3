/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

struct OscillatorView: View {
  let name: String
  let spacing: Double
  let buttonPadding: Double
  let enabled: ObservableAUParameter
  let volume: ObservableAUParameter
  let waveform: ObservableAUParameter
  let pulseWidth: ObservableAUParameter
  let pwmDepth: ObservableAUParameter
  let coarse: ObservableAUParameter
  let fineTune: ObservableAUParameter
  let vibratoDepth: ObservableAUParameter

  var body: some View {
    let padding = EdgeInsets(top: 0, leading: buttonPadding, bottom: 0, trailing: buttonPadding)

    GroupBox(label: Text(self.name)) {
      let oscEnable = ParameterDisabler(parameter: self.enabled, range: 0.0..<1.0)
      let oscEnablePulse = TwoParameterDisabler(
        parameter1: self.waveform, range1: 0.0..<2.0,
        parameter2: self.enabled, range2: 0.0..<1.0
      )

      VStack(spacing: spacing) {
        HStack(spacing: spacing) {
          ParameterPickerView(parameter: self.enabled)
            .padding(padding)
            .displayNameHidden()
          Spacer()
          ParameterView(parameter: self.volume)
            .parameterStyle(ParameterKnobStyle())
            .modifier(oscEnable)
          Spacer()
          Rectangle().hidden()
      }
      HStack(spacing: spacing) {
        ParameterPickerView(parameter: self.waveform)
          .padding(padding)
          .modifier(oscEnable)
          .displayNameHidden()

        Spacer()
        ParameterView(parameter: self.pulseWidth)
          .parameterStyle(ParameterSymmetricKnobStyle())
          .modifier(oscEnablePulse)

        Spacer()

        ParameterView(parameter: self.pwmDepth)
          .parameterStyle(ParameterKnobStyle())
          .modifier(oscEnablePulse)
      }
      HStack(spacing: spacing) {
        ParameterTextFieldView(parameter: self.coarse)
          .padding(padding)
          .modifier(oscEnable)

        Spacer()
        ParameterView(parameter: self.fineTune)
          .parameterStyle(ParameterSymmetricKnobStyle())
          .modifier(oscEnable)

        Spacer()
        ParameterView(parameter: self.vibratoDepth)
          .parameterStyle(ParameterKnobStyle())
          .modifier(oscEnable)
        }
      }
    }
  }
}
