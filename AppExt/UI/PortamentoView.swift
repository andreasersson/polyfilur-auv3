/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

struct PortamentoView: View {
  let name: String
  let spacing: Double
  let buttonPadding: Double
  let enable: ObservableAUParameter
  let time: ObservableAUParameter

  var body: some View {
    let padding = EdgeInsets(top: 0, leading: buttonPadding, bottom: 0, trailing: buttonPadding)

    GroupBox(label: Text(name)) {
      HStack(spacing: spacing) {
        ParameterView(parameter: enable)
          .toggle()
          .padding(padding)
          .displayNameHidden()
          .parameterStyle(ParameterOnOffButtonStyle())
        Spacer()
        ParameterView(parameter: time)
          .parameterStyle(ParameterKnobStyle())
        Spacer()
        Rectangle().hidden()
      }
      .padding(0)
    }
  }
}
