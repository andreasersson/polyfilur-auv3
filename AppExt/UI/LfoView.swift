/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

struct LfoView: View {
  let name: String
  let spacing: Double
  let buttonPadding: Double
  let waveform: ObservableAUParameter
  let restart: ObservableAUParameter
  let phase: ObservableAUParameter
  let speed: ObservableAUParameter
  let tempoSync: ObservableAUParameter

  let lfoSpeedParameterCurve: Float = 1.0/3.0

  var body: some View {
    let padding = EdgeInsets(top: 0, leading: buttonPadding, bottom: 0, trailing: buttonPadding)

    GroupBox(label: Text(name)) {
      VStack(spacing: spacing) {
        HStack(spacing: spacing) {
          ParameterPickerView(parameter: waveform)
            .padding(padding)
            .displayNameHidden()
          Spacer()
          ParameterView(parameter: restart)
            .toggle()
            .parameterStyle(ParameterDisplayNameButtonStyle())
            .padding(padding)
            .displayNameHidden()
          Spacer()
          ParameterView(parameter: phase)
            .parameterStyle(ParameterSymmetricKnobStyle())
            .modifier(ParameterDisabler(parameter: restart, range: 0.0..<1.0))
        }
        HStack(spacing: spacing) {
          Spacer()
          ParameterView(parameter: speed)
            .parameterStyle(ParameterKnobStyle())
            .curve(lfoSpeedParameterCurve)
            .modifier(ParameterDisabler(parameter: tempoSync, range: 1.0..<10.0))
          Spacer()
          ParameterPickerView(parameter: tempoSync)
            .padding(padding)
          Spacer()
        }
      }
    }
  }
}
