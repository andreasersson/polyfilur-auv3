/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

struct FilterView: View {
  let name: String
  let spacing: Double
  let buttonPadding: Double
  let type: ObservableAUParameter
  let keyboardFollow: ObservableAUParameter
  let cutoff: ObservableAUParameter
  let resonance: ObservableAUParameter
  let env: ObservableAUParameter
  let lfo: ObservableAUParameter
  let velocity: ObservableAUParameter
  let modWheel: ObservableAUParameter

  let resonanceParameterCurve: Float = 1.0/3.0

  var body: some View {
    let padding = EdgeInsets(top: 0, leading: buttonPadding, bottom: 0, trailing: buttonPadding)

    GroupBox(label: Text(self.name)) {
      VStack(spacing: spacing) {
        HStack(spacing: spacing) {
          ParameterPickerView(parameter: type)
            .padding(padding)
            .displayNameHidden()
          Spacer()
          Rectangle().hidden()
          Spacer()
          Rectangle().hidden()
        }
        HStack(spacing: spacing) {
          ParameterView(parameter: keyboardFollow)
            .toggle()
            .padding(padding)
            .displayNameHidden()
            .parameterStyle(ParameterDisplayNameButtonStyle())
          Spacer()
          ParameterView(parameter: cutoff)
            .parameterStyle(ParameterKnobStyle())
          Spacer()
          ParameterView(parameter: resonance)
            .parameterStyle(ParameterKnobStyle())
            .curve(resonanceParameterCurve)
        }
        HStack(spacing: spacing) {
          ParameterView(parameter: env)
            .parameterStyle(ParameterSymmetricKnobStyle())
          ParameterView(parameter: lfo)
            .parameterStyle(ParameterSymmetricKnobStyle())
          ParameterView(parameter: velocity)
            .parameterStyle(ParameterSymmetricKnobStyle())
          ParameterView(parameter: modWheel)
            .parameterStyle(ParameterSymmetricKnobStyle())
        }
      }
      .padding(0)
    }
  }
}
