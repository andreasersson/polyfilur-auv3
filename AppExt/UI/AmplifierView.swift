/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

struct AmplifierView: View {
  let name: String
  let spacing: Double
  let volume: ObservableAUParameter
  let velocityCurve: ObservableAUParameter
  let velocityDepth: ObservableAUParameter

  let velocityParameterCurve: Float = 1.0/3.0

  var body: some View {
    GroupBox(label: Text(name)) {
      VStack(spacing: spacing) {
        HStack(spacing: spacing) {
          Spacer()
          ParameterView(parameter: volume)
            .parameterStyle(ParameterKnobStyle())
          Spacer()
        }
        HStack(spacing: spacing) {
          Spacer()
          ParameterView(parameter: velocityCurve)
            .parameterStyle(ParameterSymmetricKnobStyle())
          Spacer()
          ParameterView(parameter: velocityDepth)
            .parameterStyle(ParameterKnobStyle())
            .curve(velocityParameterCurve)
          Spacer()
        }
      }
    }
  }
}
