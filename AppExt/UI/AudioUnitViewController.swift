/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import Combine
import CoreAudioKit
import SwiftUI

public class AudioUnitViewController: AUViewController, AUAudioUnitFactory {
  var audioUnit: AUAudioUnit?

  var hostingController: HostingController<PolyFilurMainView>?

  private var observation: NSKeyValueObservation?

  let aspectRatio: Double = Double(PolyFilurMainView.widthInUnits) / Double(PolyFilurMainView.heightInUnits)

  public override var preferredContentSize: CGSize {
    get {return NSSize(width: 1280, height: 1280 / aspectRatio)}
    set { super.preferredContentSize = newValue }
  }

  open override var preferredMaximumSize: NSSize { return NSSize(width: 1600, height: 1600 / aspectRatio) }
  open override var preferredMinimumSize: NSSize { return NSSize(width: 600, height: 600 / aspectRatio)}

  deinit {}

  public func createAudioUnit(with componentDescription: AudioComponentDescription) throws -> AUAudioUnit {
    audioUnit = try PolyFilur(componentDescription: componentDescription, options: [])
    guard let audioUnit = self.audioUnit as? PolyFilur else {
      return audioUnit!
    }

    defer {
      DispatchQueue.main.async {
        self.configureSwiftUIView(audioUnit: audioUnit)
      }
    }

    self.observation = audioUnit.observe(\.allParameterValues, options: [.new]) { _, _ in
      guard let tree = audioUnit.parameterTree else { return }
      for param in tree.allParameters { param.value = param.value }
    }

    guard audioUnit.parameterTree != nil else {
      return audioUnit
    }

    return audioUnit
  }

  private func configureSwiftUIView(audioUnit: AUAudioUnit) {
    if let host = hostingController {
      host.removeFromParent()
      host.view.removeFromSuperview()
    }

    guard let observableParameterTree = audioUnit.observableParameterTree else {
      return
    }
    let content = PolyFilurMainView(parameterTree: observableParameterTree)
    let host = HostingController(rootView: content)
    self.addChild(host)
    host.view.frame = self.view.bounds
    self.view.addSubview(host.view)
    hostingController = host

    host.view.translatesAutoresizingMaskIntoConstraints = false
    host.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
    host.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
    host.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    host.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    self.view.bringSubviewToFront(host.view)
  }
}
