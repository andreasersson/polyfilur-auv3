/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

private let _fontSizeScale: Double = 0.02

struct RelativeViewModifier: ViewModifier {
  var parentWidth: Int = 16
  var parentHeight: Int = 9
  var width: Int = 4
  var height: Int  = 3
  var spacing: Double = 5.0

  func body(content: Content) -> some View {
    content
      .containerRelativeFrame(.horizontal, count: self.parentWidth, span: self.width, spacing: self.spacing)
      .containerRelativeFrame(.vertical, count: self.parentHeight, span: self.height, spacing: self.spacing)
  }
}

struct PolyFilurGroupBoxStyle: GroupBoxStyle {
  var parentWidth: Int = 16
  var parentHeight: Int = 9
  var width: Int = 4
  var height: Int = 4
  var spacing: Double = 5.0

  func makeBody(configuration: Configuration) -> some View {
    VStack(spacing: 0) {
      Spacer()
      configuration.label
      Divider()
        .background(Color.windowBackgroundColor)
      Spacer()
      configuration.content
        .padding(0)
        .modifier(RelativeViewModifier(
          parentWidth: self.parentWidth,
          parentHeight: self.parentHeight,
          width: self.width,
          height: self.height - 1,
          spacing: self.spacing)
        )
      Spacer()
    }
      .modifier(RelativeViewModifier(
        parentWidth: self.parentWidth,
        parentHeight: self.parentHeight,
        width: self.width, height: self.height,
        spacing: self.spacing)
      )
  }
}

struct MiscView: View {
  let name: String
  let spacing: Double
  let buttonPadding: Double
  let voiceMode: ObservableAUParameter
  let pitchBendRange: ObservableAUParameter

  var body: some View {
    let padding = EdgeInsets(top: 0, leading: buttonPadding, bottom: 0, trailing: buttonPadding)

    GroupBox(label: Text(name)) {
      HStack(spacing: spacing) {
        ParameterPickerView(parameter: voiceMode)
          .padding(padding)
        Spacer()
          .frame(maxHeight: .infinity)
        ParameterTextFieldView(parameter: pitchBendRange)
          .padding(padding)
      }
        .padding(0)
    }
  }
}

struct PolyFilurMainView: View {
  var parameterTree: ObservableAUParameterGroup

  private static let moduleWidth = 4
  private static let module3Height = 4
  private static let module2Height = 3
  private static let module1Height = 2

  static let widthInUnits = 4 * moduleWidth
  static let heightInUnits = module3Height + module1Height + module2Height

  static let spacing: Double = 5.0
  static let buttonPadding: Double = 10.0

  let moduleStyle4x3 = PolyFilurGroupBoxStyle(
    parentWidth: widthInUnits,
    parentHeight: heightInUnits,
    width: moduleWidth,
    height: module3Height,
    spacing: spacing
  )
  let moduleStyle4x2 = PolyFilurGroupBoxStyle(
    parentWidth: widthInUnits,
    parentHeight: heightInUnits,
    width: 4,
    height: module2Height,
    spacing: spacing
  )
  let moduleStyle4x1 = PolyFilurGroupBoxStyle(
    parentWidth: widthInUnits,
    parentHeight: heightInUnits,
    width: 4,
    height: module1Height,
    spacing: spacing
  )

  let oscillator1View: OscillatorView
  let oscillator2View: OscillatorView
  let filterView: FilterView
  let amplifierView: AmplifierView
  let miscView: MiscView
  let portamentoView: PortamentoView
  let envelopeView: EnvelopeView
  let filterEnvelopeView: EnvelopeView
  let oscLfoView: LfoView
  let filterLfoView: LfoView
  let noiseView: NoiseView

  // swiftlint:disable function_body_length
  init(parameterTree: ObservableAUParameterGroup) {
    self.parameterTree = parameterTree

    oscillator1View = OscillatorView(
      name: "oscillator 1",
      spacing: PolyFilurMainView.spacing,
      buttonPadding: PolyFilurMainView.buttonPadding,
      enabled: parameterTree.global.Mixer1Ch1Enabled,
      volume: parameterTree.global.Mixer1Ch1Volume,
      waveform: parameterTree.global.Osc1Waveform,
      pulseWidth: parameterTree.global.Osc1PulseWidth,
      pwmDepth: parameterTree.global.Osc1PwmDepth,
      coarse: parameterTree.global.Osc1Coarse,
      fineTune: parameterTree.global.Osc1FineTune,
      vibratoDepth: parameterTree.global.Osc1VibratoDepth
    )

    oscillator2View = OscillatorView(
      name: "oscillator 2",
      spacing: PolyFilurMainView.spacing,
      buttonPadding: PolyFilurMainView.buttonPadding,
      enabled: parameterTree.global.Mixer1Ch2Enabled,
      volume: parameterTree.global.Mixer1Ch2Volume,
      waveform: parameterTree.global.Osc2Waveform,
      pulseWidth: parameterTree.global.Osc2PulseWidth,
      pwmDepth: parameterTree.global.Osc2PwmDepth,
      coarse: parameterTree.global.Osc2Coarse,
      fineTune: parameterTree.global.Osc2FineTune,
      vibratoDepth: parameterTree.global.Osc2VibratoDepth
    )

    filterView = FilterView(
      name: "filter",
      spacing: PolyFilurMainView.spacing,
      buttonPadding: PolyFilurMainView.buttonPadding,
      type: parameterTree.global.Filter1Type,
      keyboardFollow: parameterTree.global.Filter1KeyboardFollow,
      cutoff: parameterTree.global.Filter1Cutoff,
      resonance: parameterTree.global.Filter1Resonance,
      env: parameterTree.global.Filter1EnvDepth,
      lfo: parameterTree.global.Filter1LfoDepth,
      velocity: parameterTree.global.Filter1VelocityDepth,
      modWheel: parameterTree.global.Filter1ModWheelDepth
    )

    amplifierView = AmplifierView(
      name: "amp",
      spacing: PolyFilurMainView.spacing,
      volume: parameterTree.global.Volume,
      velocityCurve: parameterTree.global.VelocityCurve,
      velocityDepth: parameterTree.global.VelocityDepth
    )

    miscView = MiscView(
      name: " ",
      spacing: PolyFilurMainView.spacing,
      buttonPadding: PolyFilurMainView.buttonPadding,
      voiceMode: parameterTree.global.VoiceMode,
      pitchBendRange: parameterTree.global.PitchBendRange
    )

    portamentoView = PortamentoView(
      name: "portamento",
      spacing: PolyFilurMainView.spacing,
      buttonPadding: PolyFilurMainView.buttonPadding,
      enable: parameterTree.global.Portamento,
      time: parameterTree.global.PortamentoTime
    )

    envelopeView = EnvelopeView(
      name: "envelope",
      spacing: PolyFilurMainView.spacing,
      attack: parameterTree.global.Adsr1Attack,
      decay: parameterTree.global.Adsr1Decay,
      sustain: parameterTree.global.Adsr1Sustain,
      release: parameterTree.global.Adsr1Release
    )

    filterEnvelopeView = EnvelopeView(
      name: "envelope",
      spacing: PolyFilurMainView.spacing,
      attack: parameterTree.global.Adsr2Attack,
      decay: parameterTree.global.Adsr2Decay,
      sustain: parameterTree.global.Adsr2Sustain,
      release: parameterTree.global.Adsr2Release
    )

    oscLfoView = LfoView(
      name: "lfo",
      spacing: PolyFilurMainView.spacing,
      buttonPadding: PolyFilurMainView.buttonPadding,
      waveform: parameterTree.global.OscLfoWaveform,
      restart: parameterTree.global.OscLfoRestart,
      phase: parameterTree.global.OscLfoPhase,
      speed: parameterTree.global.OscLfoSpeed,
      tempoSync: parameterTree.global.OscLfoTempoSync
    )

    filterLfoView = LfoView(
      name: "lfo",
      spacing: PolyFilurMainView.spacing,
      buttonPadding: PolyFilurMainView.buttonPadding,
      waveform: parameterTree.global.FilterLfoWaveform,
      restart: parameterTree.global.FilterLfoRestart,
      phase: parameterTree.global.FilterLfoPhase,
      speed: parameterTree.global.FilterLfoSpeed,
      tempoSync: parameterTree.global.FilterLfoTempoSync
    )

    noiseView = NoiseView(
      name: "noise",
      spacing: PolyFilurMainView.spacing,
      buttonPadding: PolyFilurMainView.buttonPadding,
      enable: parameterTree.global.Mixer1Ch3Enabled,
      volume: parameterTree.global.Mixer1Ch3Volume
    )
  }
  // swiftlint:enable function_body_length

  var body: some View {
    GeometryReader { geometry in
      VStack(spacing: PolyFilurMainView.spacing) {
        HStack(alignment: .top, spacing: PolyFilurMainView.spacing) {
          oscillator1View
          oscillator2View
          filterView
          amplifierView
            .groupBoxStyle(moduleStyle4x2)
        }

        HStack(alignment: .top, spacing: PolyFilurMainView.spacing) {
          miscView
          portamentoView
            .modifier(ParameterDisabler(parameter: parameterTree.global.VoiceMode, range: 0.0..<1.0))
          filterEnvelopeView
          envelopeView
        }
          .groupBoxStyle(moduleStyle4x1)

        HStack(alignment: .top, spacing: PolyFilurMainView.spacing) {
          oscLfoView
            .groupBoxStyle(moduleStyle4x2)
          noiseView
            .groupBoxStyle(moduleStyle4x1)
          filterLfoView
            .groupBoxStyle(moduleStyle4x2)
          Rectangle().hidden()
        }
      }
        .font(.system(size: geometry.size.height * _fontSizeScale))
        .groupBoxStyle(moduleStyle4x3)
    }
      .background(Color.controlBackgroundColor)
      .environment(\.colorScheme, .dark)
  }
}
