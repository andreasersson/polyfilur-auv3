/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import Foundation
import AudioToolbox
import AVFoundation
import CoreAudioKit

// swiftlint:disable file_length
// swiftlint:disable function_body_length
func createParameterTree() -> AUParameterTree {
  var globalParameters: [AUParameterNode] = []

  let oscWaveformStrings = ["sine", "saw", "pulse"]
  let mixerStrings = ["off", "on", "bypass"]
  let filterTypeStrings = ["lp", "hp", "bp"]
  let filterKeyboardFollowStrings = ["off", "on"]
  let lfoTempoSyncStrings = ["off", "16th", "8th", "4th", "2th", "1", "2", "4"]
  let lfoWaveformStrings = ["sine", "tri", "saw"]
  let lfoRestartStrings = ["off", "on"]
  let portamentoStrings = ["off", "on"]
  let voiceModeStrings = ["poly", "mono", "legato"]

  // Oscillator 1
  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc1Waveform",
    name: "waveform",
    address: PolyFilurParameterAddress.Osc1Waveform.rawValue,
    defaultValue: 2.0,
    strings: oscWaveformStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc1PulseWidth",
    name: "pulse width",
    address: PolyFilurParameterAddress.Osc1PulseWidth.rawValue,
    range: 0.0...1.0,
    defaultValue: 0.5,
    unit: .ratio)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc1PwmDepth",
    name: "pwm",
    address: PolyFilurParameterAddress.Osc1PwmDepth.rawValue,
    range: 0.0...1.0,
    defaultValue: 0.0)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc1Coarse",
    name: "coarse",
    address: PolyFilurParameterAddress.Osc1Coarse.rawValue,
    range: -24.0...24.0,
    defaultValue: 0.0,
    unit: .relativeSemiTones)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc1FineTune",
    name: "fine tune",
    address: PolyFilurParameterAddress.Osc1FineTune.rawValue,
    range: -50.0...50.0,
    defaultValue: 0.0,
    unit: .cents)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc1VibratoDepth",
    name: "vibrato",
    address: PolyFilurParameterAddress.Osc1VibratoDepth.rawValue,
    range: 0.0...100.0,
    defaultValue: 0.0,
    unit: .cents)
  )

  // Oscillator 2
  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc2Waveform",
    name: "waveform",
    address: PolyFilurParameterAddress.Osc2Waveform.rawValue,
    defaultValue: 2.0,
    strings: oscWaveformStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc2PulseWidth",
    name: "pulse width",
    address: PolyFilurParameterAddress.Osc2PulseWidth.rawValue,
    range: 0.0...1.0,
    defaultValue: 0.5,
    unit: .ratio)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc2PwmDepth",
    name: "pwm",
    address: PolyFilurParameterAddress.Osc2PwmDepth.rawValue,
    range: 0.0...1.0,
    defaultValue: 0.0)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc2Coarse",
    name: "coarse",
    address: PolyFilurParameterAddress.Osc2Coarse.rawValue,
    range: -24.0...24.0,
    defaultValue: 0.0,
    unit: .relativeSemiTones)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc2FineTune",
    name: "fine tune",
    address: PolyFilurParameterAddress.Osc2FineTune.rawValue,
    range: -50.0...50.0,
    defaultValue: 0.0,
    unit: .cents)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Osc2VibratoDepth",
    name: "vibrato",
    address: PolyFilurParameterAddress.Osc2VibratoDepth.rawValue,
    range: 0.0...100.0,
    defaultValue: 0.0,
    unit: .cents)
  )

  // Mixer
  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Mixer1Ch1Volume",
    name: "volume",
    address: PolyFilurParameterAddress.Mixer1Ch1Volume.rawValue,
    range: -40.0...0.0,
    defaultValue: -6.0,
    unit: .decibels)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Mixer1Ch1Enabled",
    name: "oscillator 1",
    address: PolyFilurParameterAddress.Mixer1Ch1Enabled.rawValue,
    defaultValue: 1.0,
    strings: mixerStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Mixer1Ch2Volume",
    name: "volume",
    address: PolyFilurParameterAddress.Mixer1Ch2Volume.rawValue,
    range: -40.0...0.0,
    defaultValue: -6.0,
    unit: .decibels)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Mixer1Ch2Enabled",
    name: "oscillator 2",
    address: PolyFilurParameterAddress.Mixer1Ch2Enabled.rawValue,
    defaultValue: 0.0,
    strings: mixerStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Mixer1Ch3Volume",
    name: "volume",
    address: PolyFilurParameterAddress.Mixer1Ch3Volume.rawValue,
    range: -40.0...0.0,
    defaultValue: -6.0,
    unit: .decibels)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Mixer1Ch3Enabled",
    name: "noise",
    address: PolyFilurParameterAddress.Mixer1Ch3Enabled.rawValue,
    defaultValue: 0.0,
    strings: mixerStrings)
  )

  // Envelope
  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Adsr1Attack",
    name: "attack",
    address: PolyFilurParameterAddress.Adsr1Attack.rawValue,
    range: 1.0e-3...8.0,
    defaultValue: 1.0e-3,
    unit: .seconds)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Adsr1Decay",
    name: "decay",
    address: PolyFilurParameterAddress.Adsr1Decay.rawValue,
    range: 10.0e-3...10.0,
    defaultValue: 100.0e-3,
    unit: .seconds)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Adsr1Sustain",
    name: "sustain",
    address: PolyFilurParameterAddress.Adsr1Sustain.rawValue,
    range: 0.0...1.0,
    defaultValue: 1.0)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Adsr1Release",
    name: "release",
    address: PolyFilurParameterAddress.Adsr1Release.rawValue,
    range: 10.0e-3...8000.0e-3,
    defaultValue: 50.0e-3,
    unit: .seconds)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Adsr2Attack",
    name: "attack",
    address: PolyFilurParameterAddress.Adsr2Attack.rawValue,
    range: 1.0e-3...8.0,
    defaultValue: 1.0e-3,
    unit: .seconds)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Adsr2Decay",
    name: "decay",
    address: PolyFilurParameterAddress.Adsr2Decay.rawValue,
    range: 10.0e-3...10.0,
    defaultValue: 500.0e-3,
    unit: .seconds)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Adsr2Sustain",
    name: "sustain",
    address: PolyFilurParameterAddress.Adsr2Sustain.rawValue,
    range: 0.0...1.0,
    defaultValue: 0.0)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Adsr2Release",
    name: "release",
    address: PolyFilurParameterAddress.Adsr2Release.rawValue,
    range: 10.0e-3...8000.0e-3,
    defaultValue: 50.0e-3,
    unit: .seconds)
  )

  // Filter
  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Filter1Type",
    name: "type",
    address: PolyFilurParameterAddress.Filter1Type.rawValue,
    defaultValue: 0.0,
    strings: filterTypeStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Filter1KeyboardFollow",
    name: "keyboard",
    address: PolyFilurParameterAddress.Filter1KeyboardFollow.rawValue,
    defaultValue: 1.0,
    strings: filterKeyboardFollowStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Filter1Cutoff",
    name: "cutoff",
    address: PolyFilurParameterAddress.Filter1Cutoff.rawValue,
    range: 0.0...13000.0,
    defaultValue: 9000.0,
    unit: .cents)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Filter1Resonance",
    name: "resonance",
    address: PolyFilurParameterAddress.Filter1Resonance.rawValue,
    range: 0.5...10.0,
    defaultValue: 1.0)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Filter1EnvDepth",
    name: "env",
    address: PolyFilurParameterAddress.Filter1EnvDepth.rawValue,
    range: -8400.0...8400.0,
    defaultValue: 0.0,
    unit: .cents)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Filter1LfoDepth",
    name: "lfo",
    address: PolyFilurParameterAddress.Filter1LfoDepth.rawValue,
    range: -3600.0...3600.0,
    defaultValue: 0.0,
    unit: .cents)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Filter1VelocityDepth",
    name: "velocity",
    address: PolyFilurParameterAddress.Filter1VelocityDepth.rawValue,
    range: -4800.0...4800.0,
    defaultValue: 0.0,
    unit: .cents)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Filter1ModWheelDepth",
    name: "mod wheel",
    address: PolyFilurParameterAddress.Filter1ModWheelDepth.rawValue,
    range: -2400.0...2400.0,
    defaultValue: 0.0,
    unit: .cents)
  )

  // LFO
  globalParameters.append(AUParameterTree.createParameter(
    identifier: "OscLfoWaveform",
    name: "waveform",
    address: PolyFilurParameterAddress.OscLfoWaveform.rawValue,
    defaultValue: 0.0,
    strings: lfoWaveformStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "OscLfoSpeed",
    name: "speed",
    address: PolyFilurParameterAddress.OscLfoSpeed.rawValue,
    range: 0.001...20.0,
    defaultValue: 2.0,
    unit: .hertz)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "OscLfoTempoSync",
    name: "tempo",
    address: PolyFilurParameterAddress.OscLfoTempoSync.rawValue,
    defaultValue: 0.0, // TEMPO_SYNC_FREE_RUNNING
    strings: lfoTempoSyncStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "OscLfoRestart",
    name: "restart",
    address: PolyFilurParameterAddress.OscLfoRestart.rawValue,
    defaultValue: 0.0,
    strings: lfoRestartStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "OscLfoPhase",
    name: "phase",
    address: PolyFilurParameterAddress.OscLfoPhase.rawValue,
    range: -1.0...1.0,
    defaultValue: 0.0)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "FilterLfoWaveform",
    name: "waveform",
    address: PolyFilurParameterAddress.FilterLfoWaveform.rawValue,
    defaultValue: 0.0,
    strings: lfoWaveformStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "FilterLfoSpeed",
    name: "speed",
    address: PolyFilurParameterAddress.FilterLfoSpeed.rawValue,
    range: 0.001...20.0,
    defaultValue: 2.0,
    unit: .hertz)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "FilterLfoTempoSync",
    name: "tempo",
    address: PolyFilurParameterAddress.FilterLfoTempoSync.rawValue,
    defaultValue: 0.0, // TEMPO_SYNC_FREE_RUNNING
    strings: lfoTempoSyncStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "FilterLfoRestart",
    name: "restart",
    address: PolyFilurParameterAddress.FilterLfoRestart.rawValue,
    defaultValue: 0.0,
    strings: lfoRestartStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "FilterLfoPhase",
    name: "phase",
    address: PolyFilurParameterAddress.FilterLfoPhase.rawValue,
    range: -1.0...1.0,
    defaultValue: 0.0)
  )

  // Oscillators
  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Portamento",
    name: "portamento",
    address: PolyFilurParameterAddress.Portamento.rawValue,
    defaultValue: 0.0,
    strings: portamentoStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "PortamentoTime",
    name: "time",
    address: PolyFilurParameterAddress.PortamentoTime.rawValue,
    range: 1.0e-3...200.0e-3,
    defaultValue: 1.0e-3,
    unit: .seconds)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "VoiceMode",
    name: "mode",
    address: PolyFilurParameterAddress.VoiceMode.rawValue,
    defaultValue: 0.0,
    strings: voiceModeStrings)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "PitchBendRange",
    name: "bend range",
    address: PolyFilurParameterAddress.PitchBendRange.rawValue,
    range: 0.0...24.0,
    defaultValue: 12.0,
    unit: .relativeSemiTones)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "Volume",
    name: "volume",
    address: PolyFilurParameterAddress.Volume.rawValue,
    range: -40.0...0.0,
    defaultValue: -16.0,
    unit: .decibels)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "VelocityCurve",
    name: "velo curve",
    address: PolyFilurParameterAddress.VelocityCurve.rawValue,
    range: -1.0...1.0,
    defaultValue: 0.0)
  )

  globalParameters.append(AUParameterTree.createParameter(
    identifier: "VelocityDepth",
    name: "velo depth",
    address: PolyFilurParameterAddress.VelocityDepth.rawValue,
    range: 0.0...1.0,
    defaultValue: 1.0)
  )

  let global = AUParameterTree.createGroup(withIdentifier: "global", name: "global", children: globalParameters)

  return AUParameterTree.createTree(withChildren: [global])
}
// swiftlint:enable function_body_length
// swiftlint:enable file_length
