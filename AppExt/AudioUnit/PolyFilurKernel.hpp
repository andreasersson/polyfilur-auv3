/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POLYFILUR_KERNEL_HPP
#define POLYFILUR_KERNEL_HPP 0

#import "KernelBase.hpp"

#import <AudioToolbox/AudioToolbox.h>

#import <map>
#import <vector>

#import <filur/polyfilur/pf_parameters.h>
#import <filur/polyfilur/polyfilur.h>

class PolyFilurKernel : public KernelBase {
public:
  virtual ~PolyFilurKernel() {}

  virtual void initialize(int channelCount, double inSampleRate);
  virtual void deInitialize() {}
  virtual bool isBypassed();
  virtual void setBypass(bool shouldBypass);
  virtual AUAudioFrameCount maximumFramesToRender() const;
  virtual void setMaximumFramesToRender(const AUAudioFrameCount &maxFrames);
  virtual void setMusicalContextBlock(AUHostMusicalContextBlock contextBlock);
  virtual MIDIProtocolID AudioUnitMIDIProtocol() const;
  virtual void setParameter(AUParameterAddress address, AUValue value);
  virtual AUValue getParameter(AUParameterAddress address);

protected:
  virtual bool subProcess(AudioBufferList* inBufferList,
                          AudioBufferList* outBufferList,
                          AUAudioFrameCount bufferOffset,
                          AUAudioFrameCount numFramesToProcess,
                          AUEventSampleTime timestamp);
  virtual void handleEvent(AUEventSampleTime now, AURenderEvent const *event);

private:
  enum {
    MidiCCModulationWheel = 1,
    MidiCCDamperPedal = 64,
    MidiCCSostenutoPedal = 66,
    MidiCCSoftPedal = 67,
    MidiCCAllSoundOff = 120,
    MidiCCAllNotesOff = 123,
  };

  void handleParameterEvent(AUEventSampleTime now, AUParameterEvent const& parameterEvent);
  void handleMIDIEventList(AUEventSampleTime now, AUMIDIEventList const* midiEvent);
  void handleMIDI2VoiceMessage(const struct MIDIUniversalMessage& message);

  AUHostMusicalContextBlock m_musicalContextBlock;

  double m_sampleRate = 44100.0;
  double m_currentTempo = 0.0;

  std::map<uint64_t, double> m_parameters;

  bool m_bypassed = false;
  AUAudioFrameCount m_maxFramesToRender = 1024;

  polyfilur_t m_polyfilur;
  pf_parameters_t m_pf_parameters;

  std::vector<double> m_leftBuffer;
  std::vector<double> m_rightBuffer;
};

#endif // POLYFILUR_KERNEL_HPP
