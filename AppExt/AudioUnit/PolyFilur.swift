/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import Foundation
import AudioToolbox
import AVFoundation
import CoreAudioKit

extension AUParameter {
  var includeInPresets: Bool {
    return !self.flags.contains(AudioUnitParameterOptions.flag_OmitFromPresets)
  }
}

let kAUPresetParametersKey = "parameters"

public class PolyFilur: PolyFilurAudioUnit {
  let presets: FactoryPresets = FactoryPresets()
  private var _currentPreset: AUAudioUnitPreset?

  private static let parametersForOverview6: [NSNumber] = [
    NSNumber(value: PolyFilurParameterAddress.Osc1Waveform.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Osc1PulseWidth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1Cutoff.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1Resonance.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1EnvDepth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr1Decay.rawValue)
  ]
  private static let parametersForOverview8: [NSNumber] = [
    NSNumber(value: PolyFilurParameterAddress.Osc1Waveform.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Osc1PulseWidth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Osc1PwmDepth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.OscLfoSpeed.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1Cutoff.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1Resonance.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1EnvDepth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr1Decay.rawValue)
  ]
  private static let parametersForOverview10: [NSNumber] = [
    NSNumber(value: PolyFilurParameterAddress.Osc1Waveform.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Osc1PulseWidth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Osc1PwmDepth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.OscLfoSpeed.rawValue),

    NSNumber(value: PolyFilurParameterAddress.Filter1Cutoff.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1Resonance.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1EnvDepth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr1Decay.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr2Decay.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr2Sustain.rawValue)
  ]
  private static let parametersForOverview12: [NSNumber] = [
    NSNumber(value: PolyFilurParameterAddress.Osc1Waveform.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Osc1PulseWidth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Osc1PwmDepth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.OscLfoSpeed.rawValue),

    NSNumber(value: PolyFilurParameterAddress.Filter1Cutoff.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1Resonance.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Filter1EnvDepth.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr1Attack.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr1Decay.rawValue),

    NSNumber(value: PolyFilurParameterAddress.Adsr2Decay.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr2Sustain.rawValue),
    NSNumber(value: PolyFilurParameterAddress.Adsr2Release.rawValue)
  ]

  private static let parameterPriorityList: [NSNumber] = [
      NSNumber(value: PolyFilurParameterAddress.Osc1Waveform.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Osc1PulseWidth.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Osc1PwmDepth.rawValue),

      NSNumber(value: PolyFilurParameterAddress.Filter1Cutoff.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Filter1Resonance.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Filter1EnvDepth.rawValue),

      NSNumber(value: PolyFilurParameterAddress.Adsr1Attack.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Adsr1Decay.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Adsr1Sustain.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Adsr1Release.rawValue),

      NSNumber(value: PolyFilurParameterAddress.Adsr2Attack.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Adsr2Decay.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Adsr2Sustain.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Adsr2Release.rawValue),

      NSNumber(value: PolyFilurParameterAddress.Osc2Waveform.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Osc2PulseWidth.rawValue),
      NSNumber(value: PolyFilurParameterAddress.Osc2PwmDepth.rawValue)
  ]

  override init(componentDescription: AudioComponentDescription, options: AudioComponentInstantiationOptions) throws {
    try super.init(componentDescription: componentDescription, options: options)
    setupParameterTree(createParameterTree())

    // Load the first factory preset.
    let preset: AUAudioUnitPreset = AUAudioUnitPreset()
    preset.number = 0
    self.currentPreset = preset
  }

  public override func parametersForOverview(withCount count: Int) -> [NSNumber] {
    if count <= PolyFilur.parametersForOverview6.count {
        return Array(PolyFilur.parametersForOverview6[0..<min(count, PolyFilur.parametersForOverview6.count)])
    } else if count <= PolyFilur.parametersForOverview8.count {
        return Array(PolyFilur.parametersForOverview8[0..<min(count, PolyFilur.parametersForOverview8.count)])
    } else if count <= PolyFilur.parametersForOverview10.count {
        return Array(PolyFilur.parametersForOverview10[0..<min(count, PolyFilur.parametersForOverview10.count)])
    } else if count <= PolyFilur.parametersForOverview12.count {
        return Array(PolyFilur.parametersForOverview12[0..<min(count, PolyFilur.parametersForOverview12.count)])
    }

    return Array(PolyFilur.parameterPriorityList[0..<min(count, PolyFilur.parameterPriorityList.count)])
  }

  public override var factoryPresets: [AUAudioUnitPreset]? {
    return self.presets.factoryPresets()
  }

  public override var currentPreset: AUAudioUnitPreset? {
    get { return _currentPreset }
    set {
      guard let preset = newValue else {
        _currentPreset = nil
        return
      }

      // Factory preset numbers are always >= 0.
      if preset.number >= 0 {
        if let state = self.presets.state(number: preset.number) {
            self.fullState = state
            _currentPreset = preset
        }
      } else {
        do {
          fullStateForDocument = try presetState(for: preset)
          _currentPreset = preset
        } catch {}
      }
    }
  }

  public override var supportsUserPresets: Bool {
    return true
  }

  override public var fullState: [String: Any]? {
    get {
      var state = [String: Any]()
      state[kAUPresetManufacturerKey] = self.componentDescription.componentManufacturer
      state[kAUPresetTypeKey] = self.componentDescription.componentType
      state[kAUPresetSubtypeKey] = self.componentDescription.componentSubType
      state[kAUPresetVersionKey] = self.componentVersion

      if let parameterTree = self.parameterTree {
        var parameters: [String: Any] = [:]
        for parameter in parameterTree.allParameters where parameter.includeInPresets {
          parameters[String(parameter.address)] = parameter.value
        }
        state[kAUPresetParametersKey] = parameters
      }

      return state
    }
    set {
      guard let dictionary = newValue else { return }

      // 'Old' state is handled by 'super'
      if dictionary[kAUPresetParametersKey] == nil {
        super.fullState = dictionary
        return
      }

      guard let parameters = dictionary[kAUPresetParametersKey] as? [String: Any] else { return }
      for (addressString, anyValue) in parameters {
        guard let address = UInt64(addressString), let value = anyValue as? AUValue else { continue }
        guard let parameter = self.parameterTree?.parameter(withAddress: address) else { continue }

        parameter.value = value
      }
    }
  }
}
