/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "PolyFilurKernel.hpp"
#import "PolyFilurParameterAddresses.h"

#import <CoreMIDI/CoreMIDI.h>

#define WORKAROUND_REAPER_v7_11_macOS_arm64 0

void PolyFilurKernel::initialize(int channelCount, double inSampleRate) {
  m_sampleRate = inSampleRate;
  pf_parameters_init(m_sampleRate, 1.0, &m_pf_parameters);
  polyfilur_init(m_sampleRate, &m_polyfilur);

  for (const auto& [address, value] : m_parameters) {
    setParameter(address, value);
  }
}

bool PolyFilurKernel::isBypassed() {
  return m_bypassed;
}

void PolyFilurKernel::setBypass(bool shouldBypass) {
  m_bypassed = shouldBypass;
}

AUAudioFrameCount PolyFilurKernel::maximumFramesToRender() const {
  return m_maxFramesToRender;
}

void PolyFilurKernel::setMaximumFramesToRender(const AUAudioFrameCount &maxFrames) {
  m_maxFramesToRender = maxFrames;
  m_leftBuffer.resize(m_maxFramesToRender);
  m_rightBuffer.resize(m_maxFramesToRender);
}

void PolyFilurKernel::setMusicalContextBlock(AUHostMusicalContextBlock contextBlock) {
  m_musicalContextBlock = contextBlock;
}

MIDIProtocolID PolyFilurKernel::AudioUnitMIDIProtocol() const {
  return kMIDIProtocol_2_0;
}

void PolyFilurKernel::setParameter(AUParameterAddress address, AUValue value) {
  m_parameters[address] = value;

  switch (address) {
    // Oscillator 1
    case PolyFilurParameterAddress::Osc1Waveform:
      pf_osc_parameters_set_waveform(static_cast<pf_osc_waveform_t>(value), &m_pf_parameters.osc1);
      break;

    case PolyFilurParameterAddress::Osc1PulseWidth:
      pf_osc_parameters_set_pulse_width(value, &m_pf_parameters.osc1);
      break;

    case PolyFilurParameterAddress::Osc1PwmDepth:
      pf_osc_parameters_set_pwm_depth(value, &m_pf_parameters.osc1);
      break;

    case PolyFilurParameterAddress::Osc1Coarse:
      pf_osc_parameters_set_coarse(100.0 * value, &m_pf_parameters.osc1);
      break;

    case PolyFilurParameterAddress::Osc1FineTune:
      pf_osc_parameters_set_fine_tune(value, &m_pf_parameters.osc1);
      break;

    case PolyFilurParameterAddress::Osc1VibratoDepth:
      pf_osc_parameters_set_vibrato_depth(value, &m_pf_parameters.osc1);
      break;

    // Oscillator 2
    case PolyFilurParameterAddress::Osc2Waveform:
      pf_osc_parameters_set_waveform(static_cast<pf_osc_waveform_t>(value), &m_pf_parameters.osc2);
      break;

    case PolyFilurParameterAddress::Osc2PulseWidth:
      pf_osc_parameters_set_pulse_width(value, &m_pf_parameters.osc2);
      break;

    case PolyFilurParameterAddress::Osc2PwmDepth:
      pf_osc_parameters_set_pwm_depth(value, &m_pf_parameters.osc2);
      break;

    case PolyFilurParameterAddress::Osc2Coarse:
      pf_osc_parameters_set_coarse(100.0 * value, &m_pf_parameters.osc2);
      break;

    case PolyFilurParameterAddress::Osc2FineTune:
      pf_osc_parameters_set_fine_tune(value, &m_pf_parameters.osc2);
      break;

    case PolyFilurParameterAddress::Osc2VibratoDepth:
      pf_osc_parameters_set_vibrato_depth(value, &m_pf_parameters.osc2);
      break;

    case PolyFilurParameterAddress::Mixer1Ch1Volume:
      pf_mixer_ch_parameters_set_volume(value, &m_pf_parameters.mixer.ch1);
      break;

    case PolyFilurParameterAddress::Mixer1Ch1Enabled:
      pf_mixer_ch_parameters_set_enabled(static_cast<pf_mixer_ch_state_t>(value), &m_pf_parameters.mixer.ch1);
      break;

    case PolyFilurParameterAddress::Mixer1Ch2Volume:
      pf_mixer_ch_parameters_set_volume(value, &m_pf_parameters.mixer.ch2);
      break;

    case PolyFilurParameterAddress::Mixer1Ch2Enabled:
      pf_mixer_ch_parameters_set_enabled(static_cast<pf_mixer_ch_state_t>(value), &m_pf_parameters.mixer.ch2);
      break;

    case PolyFilurParameterAddress::Mixer1Ch3Volume:
      pf_mixer_ch_parameters_set_volume(value, &m_pf_parameters.mixer.ch3);
      break;

    case PolyFilurParameterAddress::Mixer1Ch3Enabled:
      pf_mixer_ch_parameters_set_enabled(static_cast<pf_mixer_ch_state_t>(value), &m_pf_parameters.mixer.ch3);
      break;

    // Envelope
    case PolyFilurParameterAddress::Adsr1Attack:
      adsr_set_attack(value, &m_pf_parameters.adsr1);
      break;

    case PolyFilurParameterAddress::Adsr1Decay:
      adsr_set_decay(value, &m_pf_parameters.adsr1);
      break;

    case PolyFilurParameterAddress::Adsr1Sustain:
      adsr_set_sustain(value, &m_pf_parameters.adsr1);
      break;

    case PolyFilurParameterAddress::Adsr1Release:
      adsr_set_release(value, &m_pf_parameters.adsr1);
      break;

    case PolyFilurParameterAddress::Adsr2Attack:
      adsr_set_attack(value, &m_pf_parameters.adsr2);
      break;

    case PolyFilurParameterAddress::Adsr2Decay:
      adsr_set_decay(value, &m_pf_parameters.adsr2);
      break;

    case PolyFilurParameterAddress::Adsr2Sustain:
      adsr_set_sustain(value, &m_pf_parameters.adsr2);
      break;

    case PolyFilurParameterAddress::Adsr2Release:
      adsr_set_release(value, &m_pf_parameters.adsr2);
      break;

    case PolyFilurParameterAddress::Filter1Type:
      pf_filter_parameters_set_type(static_cast<pf_filter_type_t>(value), &m_pf_parameters.filter);
      break;

    case PolyFilurParameterAddress::Filter1KeyboardFollow:
      pf_filter_parameters_set_kbf_enabled(static_cast<pf_kbf_enabled_t>(value), &m_pf_parameters.filter);
      break;

    case PolyFilurParameterAddress::Filter1Cutoff:
      pf_filter_parameters_set_cutoff(value, &m_pf_parameters.filter);
      break;

    case PolyFilurParameterAddress::Filter1Resonance:
      pf_filter_parameters_set_resonance(value, &m_pf_parameters.filter);
      break;

    case PolyFilurParameterAddress::Filter1EnvDepth:
      pf_filter_parameters_set_env_depth(value, &m_pf_parameters.filter);
      break;

    case PolyFilurParameterAddress::Filter1LfoDepth:
      pf_filter_parameters_set_lfo_depth(value, &m_pf_parameters.filter);
      break;

    case PolyFilurParameterAddress::Filter1VelocityDepth:
      pf_filter_parameters_set_velocity_depth(value, &m_pf_parameters.filter);
      break;

    case PolyFilurParameterAddress::Filter1ModWheelDepth:
      pf_filter_parameters_set_mod_wheel_depth(value, &m_pf_parameters.filter);
      break;

    case PolyFilurParameterAddress::OscLfoWaveform:
      pf_lfo_parameters_set_waveform(static_cast<pf_lfo_waveform_t>(value), &m_pf_parameters.osc_lfo);
      break;

    case PolyFilurParameterAddress::OscLfoSpeed:
      pf_lfo_parameters_set_speed(value, &m_pf_parameters.osc_lfo);
      pf_parameters_update_lfo_speed(m_pf_parameters.tempo, &m_pf_parameters.osc_lfo);
      break;

    case PolyFilurParameterAddress::OscLfoTempoSync:
      pf_lfo_parameters_set_tempo_sync(static_cast<tempo_sync_t>(value), &m_pf_parameters.osc_lfo);
      pf_parameters_update_lfo_speed(m_pf_parameters.tempo, &m_pf_parameters.osc_lfo);
      break;

    case PolyFilurParameterAddress::OscLfoRestart:
      pf_lfo_parameters_set_restart(value < 0.5f ? PF_LFO_RESTART_FALSE : PF_LFO_RESTART_TRUE, &m_pf_parameters.osc_lfo);
      break;

    case PolyFilurParameterAddress::OscLfoPhase:
      pf_lfo_parameters_set_phase(value, &m_pf_parameters.osc_lfo);
      break;

    case PolyFilurParameterAddress::FilterLfoWaveform:
      pf_lfo_parameters_set_waveform(static_cast<pf_lfo_waveform_t>(value), &m_pf_parameters.filter_lfo);
      break;

    case PolyFilurParameterAddress::FilterLfoSpeed:
      pf_lfo_parameters_set_speed(value, &m_pf_parameters.filter_lfo);
      pf_parameters_update_lfo_speed(m_pf_parameters.tempo, &m_pf_parameters.filter_lfo);
      break;

    case PolyFilurParameterAddress::FilterLfoTempoSync:
      pf_lfo_parameters_set_tempo_sync(static_cast<tempo_sync_t>(value), &m_pf_parameters.filter_lfo);
      pf_parameters_update_lfo_speed(m_pf_parameters.tempo, &m_pf_parameters.filter_lfo);
      break;

    case PolyFilurParameterAddress::FilterLfoRestart:
      pf_lfo_parameters_set_restart(value < 0.5f ? PF_LFO_RESTART_FALSE : PF_LFO_RESTART_TRUE, &m_pf_parameters.filter_lfo);
      break;

    case PolyFilurParameterAddress::FilterLfoPhase:
      pf_lfo_parameters_set_phase(value, &m_pf_parameters.filter_lfo);
      break;

    case PolyFilurParameterAddress::Portamento:
      m_pf_parameters.portamento.enabled = value < 0.5f ? PF_PORTAMENTO_DISABLED : PF_PORTAMENTO_ENABLED;
      break;

    case PolyFilurParameterAddress::PortamentoTime:
      m_pf_parameters.portamento.time = value;
      break;

    case PolyFilurParameterAddress::VoiceMode:
      pf_parameters_set_voice_mode(static_cast<pf_voice_mode_t>(value), &m_pf_parameters);
      break;

    case PolyFilurParameterAddress::PitchBendRange:
      pf_parameters_set_pitch_bend_range(value, &m_pf_parameters);
      break;

    case PolyFilurParameterAddress::Volume:
      pf_parameters_set_volume(value, &m_pf_parameters);
      break;

    case PolyFilurParameterAddress::VelocityDepth:
      pf_parameters_set_velocity_depth(std::min(1.0f, std::max(-1.0f, value)), &m_pf_parameters);
      break;

    case PolyFilurParameterAddress::VelocityCurve:
      pf_parameters_set_velocity_curve(std::min(1.0f, std::max(-1.0f, value)), &m_pf_parameters);
      break;
  }
}

AUValue PolyFilurKernel::getParameter(AUParameterAddress address) {
  switch (address) {
      default: return m_parameters[address];
  }
}

bool PolyFilurKernel::subProcess(AudioBufferList* inBufferList,
                                 AudioBufferList* outBufferList,
                                 AUAudioFrameCount bufferOffset,
                                 AUAudioFrameCount numFramesToProcess,
                                 AUEventSampleTime timestamp) {
  float* leftOut = ((float*)outBufferList->mBuffers[0].mData) + bufferOffset;
  float* rightOut = ((float*)outBufferList->mBuffers[1].mData) + bufferOffset;
  bool isSilent = true;

  if (m_bypassed) {
    for (size_t n = 0u; n < numFramesToProcess; ++n) {
      leftOut[n] = 0.0f;
      rightOut[n] = 0.0f;
    }

    return isSilent;
  }

  if (m_musicalContextBlock) {
    double currentTempo = 120.0;
    double timeSignatureNumerator = 4.0;
    long timeSignatureDenominator = 4;
#ifdef WORKAROUND_REAPER_v7_11_macOS_arm64
    /*
    * NOTE: This is a workaround for REAPER v7.11-macOS-arm64 crasching when calling musicalContextBlock with null pointers.
    * From the AUHostMusicalContextBlock documtentation,
    * https://developer.apple.com/documentation/audiotoolbox/auhostmusicalcontextblock?language=objc
    * "Any of the provided parameters may be null to indicate that the audio unit is not interested in that particular piece of information."
    */
    double currentBeatPosition = 0.0;
    NSInteger sampleOffsetToNextBeat = 0;
    double currentMeasureDownbeatPosition = 0.0;

    bool result = m_musicalContextBlock(&currentTempo,
                                        &timeSignatureNumerator,
                                        &timeSignatureDenominator,
                                        &currentBeatPosition,
                                        &sampleOffsetToNextBeat,
                                        &currentMeasureDownbeatPosition);
#else
    bool result = m_musicalContextBlock(&currentTempo,
                                        &timeSignatureNumerator,
                                        &timeSignatureDenominator,
                                        nullptr /* currentBeatPosition */,
                                        nullptr /* sampleOffsetToNextBeat */,
                                        nullptr /* currentMeasureDownbeatPosition */);
#endif
    if (result && (m_currentTempo != currentTempo)) {
      m_currentTempo = currentTempo;
      pf_parameters_set_tempo(m_currentTempo, &m_pf_parameters);
    }
  }

  for (size_t n = 0u; n < numFramesToProcess; ++n) {
    m_leftBuffer[n] = 0.0f;
    m_rightBuffer[n] = 0.0f;
  }

  isSilent &= (polyfilur_num_active_voices(&m_polyfilur) == 0);
  polyfilur_process(m_leftBuffer.data(), m_rightBuffer.data(), numFramesToProcess, &m_pf_parameters, &m_polyfilur);
  isSilent &= (polyfilur_num_active_voices(&m_polyfilur) == 0);

  for (size_t n = 0u; n < numFramesToProcess; ++n) {
    leftOut[n] = static_cast<float>(m_leftBuffer[n]);
    rightOut[n] = static_cast<float>(m_rightBuffer[n]);
  }

  return isSilent;
}

void PolyFilurKernel::handleEvent(AUEventSampleTime now, AURenderEvent const *event) {
  switch (event->head.eventType) {
    case AURenderEventParameter:
      handleParameterEvent(now, event->parameter);
      break;

    case AURenderEventMIDIEventList:
      handleMIDIEventList(now, &event->MIDIEventsList);
      break;

    case AURenderEventMIDI:
      break;

    default:
      break;
  }
}

void PolyFilurKernel::handleParameterEvent(AUEventSampleTime now, AUParameterEvent const& parameterEvent) {
}

void PolyFilurKernel::handleMIDIEventList(AUEventSampleTime now, AUMIDIEventList const* midiEvent) {
  auto visitor = [] (void* context, MIDITimeStamp timeStamp, MIDIUniversalMessage message) {
    auto kernel = static_cast<PolyFilurKernel*>(context);

    switch (message.type) {
      case kMIDIMessageTypeChannelVoice2:
        kernel->handleMIDI2VoiceMessage(message);
        break;

      default:
        break;
    }
  };

  MIDIEventListForEachEvent(&midiEvent->eventList, visitor, this);
}

void PolyFilurKernel::handleMIDI2VoiceMessage(const struct MIDIUniversalMessage& message) {
  static const double normalize32 = 1.0 / double(std::numeric_limits<uint32_t>::max());
  static const double normalize16 = 1.0 / (double)std::numeric_limits<std::uint16_t>::max();

  const auto& note = message.channelVoice2.note;

  switch (message.channelVoice2.status) {
    case kMIDICVStatusNoteOff:
      polyfilur_note_off(note.number, &m_pf_parameters, &m_polyfilur);
      break;

    case kMIDICVStatusNoteOn:
      polyfilur_note_on(note.number, (double)message.channelVoice2.note.velocity * normalize16, &m_pf_parameters, &m_polyfilur);
      break;

    case kMIDICVStatusControlChange: {
      switch (message.channelVoice2.controlChange.index) {
        case MidiCCModulationWheel:
          pf_parameters_set_mod_wheel(message.channelVoice2.controlChange.data * normalize32, &m_pf_parameters);
          break;
// TODO: Should we support MidiCCDamperPedal, MidiCCSostenutoPedal and MidiCCSoftPedal?
#if 0
        case MidiCCDamperPedal:
            break;

        case MidiCCSostenutoPedal:
            break;

        case MidiCCSoftPedal:
            break;
#endif
        case MidiCCAllSoundOff:
        case MidiCCAllNotesOff:
          polyfilur_all_notes_off(&m_pf_parameters, &m_polyfilur);
          break;

        default:
          break;
      }
      break;
    }

    case kMIDICVStatusPitchBend:
      pf_parameters_set_pitch_bend(message.channelVoice2.pitchBend.data * normalize32, &m_pf_parameters);
      break;

    default:
      break;
  }
}
