/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of polyfilur-auv3.
 *
 * polyfilur-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POLYFILUR_PARAMETER_ADDRESSES_H
#define POLYFILUR_PARAMETER_ADDRESSES_H 0

#include <AudioToolbox/AUParameters.h>

#ifdef __cplusplus
namespace PolyFilurParameterAddress {
#endif

typedef NS_ENUM(AUParameterAddress, PolyFilurParameterAddress) {
  Osc1Waveform = 0,
  Osc1PulseWidth,
  Osc1PwmDepth,
  Osc1Coarse,
  Osc1FineTune,
  Osc1VibratoDepth,

  Osc2Waveform = 10,
  Osc2PulseWidth,
  Osc2PwmDepth,
  Osc2Coarse,
  Osc2FineTune,
  Osc2VibratoDepth,

  Filter1Type = 20,
  Filter1KeyboardFollow,
  Filter1Cutoff,
  Filter1Resonance,
  Filter1EnvDepth,
  Filter1LfoDepth,
  Filter1VelocityDepth,
  Filter1ModWheelDepth,

  Mixer1Ch1Volume = 30,
  Mixer1Ch1Enabled,
  Mixer1Ch2Volume,
  Mixer1Ch2Enabled,
  Mixer1Ch3Volume,
  Mixer1Ch3Enabled,

  Adsr1Attack = 40,
  Adsr1Decay,
  Adsr1Sustain,
  Adsr1Release,

  Adsr2Attack = 50,
  Adsr2Decay,
  Adsr2Sustain,
  Adsr2Release,

  OscLfoWaveform = 60,
  OscLfoSpeed,
  OscLfoTempoSync,
  OscLfoRestart,
  OscLfoPhase,

  FilterLfoWaveform = 70,
  FilterLfoSpeed,
  FilterLfoTempoSync,
  FilterLfoRestart,
  FilterLfoPhase,

  Portamento = 80,
  PortamentoTime,

  Volume = 90,
  VelocityDepth,
  VelocityCurve,

  VoiceMode = 100,
  PitchBendRange
};

#ifdef __cplusplus
}
#endif

#endif // POLYFILUR_PARAMETER_ADDRESSES_H
