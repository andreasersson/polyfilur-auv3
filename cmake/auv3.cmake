include(FetchContent)

FetchContent_Declare(
  auv3
  GIT_REPOSITORY https://gitlab.com/andreasersson/auv3.git
  GIT_TAG        main
)

FetchContent_GetProperties(auv3)
if(NOT auv3_POPULATED)
  FetchContent_Populate(auv3)
endif()
