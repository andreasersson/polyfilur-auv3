include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/version.cmake)

# create arg list to be used with cmake_parse_arguments
foreach(argc RANGE ${CMAKE_ARGC})
    list(APPEND argn ";${CMAKE_ARGV${argc}}")
endforeach(argc)

# parse input arguments
cmake_parse_arguments(CHECK_VER "" "VERSION;REPOSITORY;TARGET;TEST_VERSION" "" ${argn})

if((DEFINED CHECK_VER_REPOSITORY) AND (DEFINED CHECK_VER_TARGET))
    check_version(${CHECK_VER_REPOSITORY} ${CHECK_VER_VERSION} result ${CHECK_VER_TEST_VERSION})

    if(NOT ${result})
        execute_process(COMMAND ${CMAKE_COMMAND} -E touch ${CHECK_VER_TARGET})
    endif()
endif()
