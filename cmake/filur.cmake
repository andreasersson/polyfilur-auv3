include(FetchContent)

set(target filur)

FetchContent_Declare(
  ${target}
  GIT_REPOSITORY https://gitlab.com/andreasersson/filur.git
  GIT_TAG        v0.4
)

FetchContent_GetProperties(${target})
if(NOT ${target}_POPULATED)
  FetchContent_Populate(${target})

  add_subdirectory(${${target}_SOURCE_DIR} ${${target}_BINARY_DIR})
endif()

include_directories(${${target}_SOURCE_DIR}/include)
