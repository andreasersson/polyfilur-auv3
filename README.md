# AUv3 Virtual analog synthesizer

<img src="https://gitlab.com/andreasersson/polyfilur-auv3/-/wikis/uploads/8203ecc628009b1636a33133aec7c1d8/PolyFilur.png" alt="polyfilur screenshot" width="800"/>

## Requirements
- [XCode]
- [CMake][CMake] 3.28 or later.

## Dependencies
- [filur][filur]
- [auv3][auv3]

## Build for MacOS.
Download the source or clone the git repository.

    git clone https://gitlab.com/andreasersson/polyfilur-auv3.git

*Note that it is currently only possible to build using Xcode as a generator.*

    cmake -S polyfilur-auv3 -B build-polyfilur-auv3 -G Xcode
    cmake --build build-polyfilur-auv3 --config Release

### Code signing
By default, the application, application extension and the framework will be singed using ad hoc signing.  
If you want to sign the build using you own certificate, set CMAKE_XCODE_ATTRIBUTE_DEVELOPMENT_TEAM and CMAKE_XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY on the command line during configuration.  
example:

    cmake -DCMAKE_XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY="Apple Development" -DCMAKE_XCODE_ATTRIBUTE_DEVELOPMENT_TEAM="A1B2C3D4E5" -S polyfilur-auv3 -B build-polyfilur-auv3 -G Xcode

## Installation
*Note that the build system will automatically register the application and the application extension in the system.*

Start the container application, PolyFilur, to register the application extension in the system.

## License
[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png "GNU General Public License")](https://www.gnu.org/licenses/gpl.html)

    AUv3Polyfilur is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AUv3Polyfilur is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

[Xcode]: https://developer.apple.com/xcode/
[CMake]: https://cmake.org/
[filur]: https://gitlab.com/andreasersson/filur
[auv3]: https://gitlab.com/andreasersson/auv3
